package lab.patterns.soccer.match;


import lab.patterns.soccer.team.Team;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Match {

    private Team team1;

    private Team team2;

    private Map<Team, Integer> result = new HashMap<>();


    public Match(Team team1, Team team2) {
        this.team1 = team1;
        this.team2 = team2;
        result.put(team1, 0);
        result.put(team2, 0);

    }

    public void teamPresentation(){
        System.out.println("[ Presenting teams ]\n");
        System.out.println(team1.getName() + " vs " + team2.getName() + "\n");
        System.out.println(team1.presentYourself());
        System.out.println(team2.presentYourself());

    }


    public void score(Team t){
        Integer r = result.get(t);
        result.put(t, ++r);
    }


    public Team getTeam1() {
        return team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public Map<Team, Integer> getResult() {
        return result;
    }


    public void play(){

        int eventCount = new Random().nextInt(100);
        for(int i=0; i<=eventCount; i++){
            Event e = EventGenerator.generateEvent(this);
            getHandlerChain().handleEvent(e);
        }
    }

    private EventHandler getHandlerChain(){
        return null;
    }


    public void printResult() {
        System.out.println("\n" + team1.getName() + " : " + team2.getName());
        System.out.println(result.get(team1) + " : " + result.get(team2));

        int team1result = result.get(team1);
        int team2result = result.get(team2);

        if(team1result==team2result){
            System.out.println("There was a draw");
        } else {
            System.out.println("Winning team: \n" + (team1result>team2result? team1 : team2));
        }


    }




}
