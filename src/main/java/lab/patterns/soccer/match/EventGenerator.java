package lab.patterns.soccer.match;

import lab.patterns.soccer.team.Player;
import lab.patterns.soccer.team.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class EventGenerator {

    public static Event generateEvent(Match m){
        EventType type = randomEventType();
        Team randomTeam = randomTeam(m.getTeam1(),m.getTeam2());
        Player p = randomPlayer(randomTeam, type!= EventType.GOAL);
        return new Event(type,m,randomTeam,p);
    }

    private static EventType randomEventType(){
        int eventIndex = ThreadLocalRandom.current().nextInt(EventType.values().length);
        return EventType.values()[eventIndex];
    }

    private static Team randomTeam(Team team1, Team team2){
        boolean random = ThreadLocalRandom.current().nextBoolean();
        return ThreadLocalRandom.current().nextBoolean() ? team1 : team2;
    }

    private static Player randomPlayer(Team t, boolean includeGoalKeeper){
        List<Player> players = new ArrayList<>();
        players.addAll(t.getDefenders());
        players.addAll(t.getMidfields());
        players.addAll(t.getAttackers());
        if(includeGoalKeeper && t.getGoalKeeper()!=null){
            players.add(t.getGoalKeeper());
        }
        int playerIndex = ThreadLocalRandom.current().nextInt(players.size());
        return players.get(playerIndex);
    }

}
