package lab.patterns.soccer.match;

public abstract class EventHandler {

    private EventHandler next;

    public void handleEvent(Event e){
        if(next!=null){
            next.handleEvent(e);
        }
    }
}
