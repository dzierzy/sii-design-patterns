package lab.patterns.soccer.team;

public class Player {

    private int rank;

    private boolean playsAbroad;

    private String firstName;

    private String lastName;

    public Player(String firstName, String lastName, int rank, boolean playsAbroad) {
        this.rank = rank;
        this.playsAbroad = playsAbroad;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getRank() {
        return rank;
    }

    public boolean isPlaysAbroad() {
        return playsAbroad;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "{" + lastName + ':' + firstName + ':' + rank + ":" + playsAbroad + "}";
    }
}
