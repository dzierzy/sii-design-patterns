package lab.patterns.soccer.team;

import java.util.List;

public interface AvailablePlayers {

    List<Player> getAllGoalKeepers();

    List<Player> getAllDefenders();

    List<Player> getAllMidfields();

    List<Player> getAllAttackers();


}
