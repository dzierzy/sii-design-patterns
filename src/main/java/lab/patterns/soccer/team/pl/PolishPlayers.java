package lab.patterns.soccer.team.pl;



import lab.patterns.soccer.team.AvailablePlayers;
import lab.patterns.soccer.team.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PolishPlayers implements AvailablePlayers {

    private static PolishPlayers instance = new PolishPlayers();

    private PolishPlayers(){}

    public static PolishPlayers getInstance() {
        return instance;
    }



    public List<Player> goalKeepers = new ArrayList<>(Arrays.asList(
            new Player("Wojciech", "Szczesny", 6, true),
            new Player("Lukasz", "Fabianski", 9, true),
            new Player("Artur", "Boruc", 15, true),
            new Player("Grzegorz", "Szamotulski", 28, false)
    ));

    public List<Player> defenders = new ArrayList<>(Arrays.asList(
            new Player("Michal", "Pazdan", 120, false),
            new Player("Thiago", "Cionek", 122, true),
            new Player("Jakub", "Wawrzyniak", 88, false),
            new Player("Kamil", "Glik", 12, true),
            new Player("Bartosz", "Salamon", 99, true),
            new Player("Lukasz", "Piszczek", 14, true),
            new Player("Artur", "Jedrzejczyk", 110, false),
            new Player("Arkadiusz", "Glowacki", 120, false),
            new Player("Jakub", "Rzezniczak", 130, false)
    ));

    public List<Player> midfields = new ArrayList<>(Arrays.asList(
            new Player("Krzysztof", "Maczynski", 120, false),
            new Player("Tomasz", "Jodlowiec", 122, false),
            new Player("Karol", "Linetty", 88, false),
            new Player("Grzegorz", "Krychowiak", 12, true),
            new Player("Kamil", "Grosicki", 99, true),
            new Player("Jakub", "Blaszczykowski", 14, true),
            new Player("Slawomir", "Peszko", 110, true),
            new Player("Piotr", "Zielinski", 120, true),
            new Player("Bartosz", "Kapustka", 130, false),
            new Player("Filip", "Starzynski", 130, false)
    ));

    public List<Player> attackers = new ArrayList<>(Arrays.asList(
            new Player("Arkadiusz", "Milik", 9, true),
            new Player("Robert", "Lewandowski", 2, true),
            new Player("Mariusz", "Stepinski", 54, false),
            new Player("Zbigniew", "Boniek", 12, false),
            new Player("Euzebiusz", "Smolarek", 19, false)
    ));


    @Override
    public List<Player> getAllGoalKeepers() {
        return this.goalKeepers;
    }

    @Override
    public List<Player> getAllDefenders() {
        return defenders;
    }

    @Override
    public List<Player> getAllMidfields() {
        return midfields;
    }

    @Override
    public List<Player> getAllAttackers() {
        return attackers;
    }
}
