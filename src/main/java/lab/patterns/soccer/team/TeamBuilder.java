package lab.patterns.soccer.team;

import java.util.List;

public class TeamBuilder {

    private String name;

    private Player goalKeeper;

    private List<Player> defenders;

    private List<Player> midfields;

    private List<Player> attackers;

    TeamBuilder addGoalKeeper(Player goalKeeper){
        this.goalKeeper = goalKeeper;
        return this;
    }

    TeamBuilder addDefenders(List<Player> defenders){
        this.defenders = defenders;
        return this;
    }

    TeamBuilder addMidfields(List<Player> midfields){
        this.midfields = midfields;
        return this;
    }

    TeamBuilder addAttackers(List<Player> attackers){
        this.attackers = attackers;
        return this;
    }

    Team makeEleven(){
        Team team = new Team();
        team.setName(name);
        team.setGoalKeeper(goalKeeper);
        team.setDefenders(defenders);
        team.setMidfields(midfields);
        team.setAttackers(attackers);
        return team;
    }

    public TeamBuilder addName(String name) {
        this.name = name;
        return this;
    }
}