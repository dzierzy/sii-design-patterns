package lab.patterns.soccer.team;

import lab.patterns.soccer.team.selection.PlayersProvider;

import java.util.List;

public class Coach {

    protected AvailablePlayers players;

    private String name;


    public Coach(String name, AvailablePlayers players) {
        this.players = players;
        this.name = name;
    }

    public Team constructTeam(){

        Team team = new TeamBuilder()
                .addName(name)
                .addGoalKeeper(PlayersProvider.findByRank(players.getAllGoalKeepers(),1,true).get(0))
                .addDefenders(PlayersProvider.findByRank(players.getAllDefenders(),4,true))
                .addMidfields(PlayersProvider.findByRank(players.getAllMidfields(),4,true))
                .addAttackers(PlayersProvider.findByRank(players.getAllAttackers(),2,true))
                .makeEleven(); // build
        return team;
    }

}
