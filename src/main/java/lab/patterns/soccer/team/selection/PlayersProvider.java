package lab.patterns.soccer.team.selection;



import lab.patterns.soccer.team.Player;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public  class PlayersProvider {



    public static Player findByName(List<Player> players, String lastName){
        return players.stream().filter(p -> p.getLastName().equalsIgnoreCase(lastName)).findFirst().get();
    }

    public static List<Player> findByRank(List<Player> players, int amount, boolean includePlayingAbroad){
        Stream<Player> stream = players.stream();
        if(!includePlayingAbroad){
            stream = stream.filter(p -> !p.isPlaysAbroad());
        }
        return stream.sorted((p1, p2) -> p1.getRank()-p2.getRank()).limit(amount).collect(Collectors.toList());
    }
}
