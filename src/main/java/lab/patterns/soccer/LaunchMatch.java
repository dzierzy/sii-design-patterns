package lab.patterns.soccer;


import lab.patterns.soccer.match.Match;
import lab.patterns.soccer.team.Coach;
import lab.patterns.soccer.team.Team;
import lab.patterns.soccer.team.ger.GermanPlayers;
import lab.patterns.soccer.team.pl.PolishPlayers;

/**
 * Created by xdzm on 2016-06-16.
 */
public class LaunchMatch {

    public static void main(String[] args) {

        Coach nawalka = new Coach("Polska", PolishPlayers.getInstance());
        Team polska = nawalka.constructTeam();

        Coach loew = new Coach("Niemcy", GermanPlayers.getInstance());
        Team niemcy = loew.constructTeam();

        Match match = new Match(polska, niemcy);

        match.teamPresentation();
        match.play();
        match.printResult();
    }
}
