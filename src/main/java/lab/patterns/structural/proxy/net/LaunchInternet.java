package lab.patterns.structural.proxy.net;

import lab.patterns.structural.proxy.net.internet.InternetProvider;

public class LaunchInternet {

    public static void main(String[] args) {
        System.out.println("lets surf!");

        Internet internet = InternetProvider.getInternet();

        internet.connectTo("wp.pl/facebook.com");
        internet.connectTo("facebook.com/events");

    }
}
