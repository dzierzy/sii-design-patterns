package lab.patterns.structural.proxy.net.internet;

import lab.patterns.structural.proxy.net.Internet;

import java.util.ArrayList;
import java.util.List;

public class FilteredNetwork implements Internet {

    private Internet realNetwork;

    private List<String> blackList = new ArrayList<>();

    private Internet getRealNetwork() {
        if (realNetwork == null)
            realNetwork = new RealNetwork();
        return realNetwork;
    }

    FilteredNetwork() {
        blackList.add("facebook.com");
    }


    @Override
    public void connectTo(final String address) {
        getRealNetwork().connectTo(
                blackList.stream().anyMatch(blAddres -> address.startsWith(blAddres)) ? "warning.com" : address
        );
    }
}
