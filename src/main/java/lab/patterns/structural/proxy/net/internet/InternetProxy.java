package lab.patterns.structural.proxy.net.internet;

import lab.patterns.structural.proxy.net.Internet;

import java.util.Arrays;
import java.util.List;

public class InternetProxy implements Internet {
    Internet internet;
    List<String> blockedList = Arrays.asList("facebook.com/events");


    private Internet getRealNetwork() {
        if (internet == null)
            internet = new RealNetwork();
        return internet;
    }

    @Override
    public void connectTo(String address) {

        if (blockedList.contains(address)) {
            System.out.println("go back to work to work");
        } else {
            internet = getRealNetwork();
            internet.connectTo(address);
        }
    }
}
