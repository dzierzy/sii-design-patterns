package lab.patterns.structural.proxy.net.internet;

import lab.patterns.structural.proxy.net.Internet;

public class RealNetwork implements Internet {


    RealNetwork(){}

    @Override
    public void connectTo(String address) {
        System.out.println("connecting to " + address);
    }
}
