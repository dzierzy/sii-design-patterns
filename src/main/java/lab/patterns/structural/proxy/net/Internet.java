package lab.patterns.structural.proxy.net;

public interface Internet {


    void connectTo(String address);
}
