package lab.patterns.structural.proxy.net.internet;

import lab.patterns.structural.proxy.net.Internet;

public class InternetProvider {

    public static Internet getInternet(){
        return new FilteredNetwork();
    }

}
