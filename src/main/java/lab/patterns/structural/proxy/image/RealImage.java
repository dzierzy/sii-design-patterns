package lab.patterns.structural.proxy.image;

public class RealImage implements Image {

    byte[] imageData;

    public RealImage(String imageFileName){
        System.out.println("RealImage: loading data from " + imageFileName);
        imageData = new byte[1024]; // emulate loding data from imageFileName
    }

    @Override
    public void display() {
        System.out.println("displaing image"); // emulate display imageData
    }
}
