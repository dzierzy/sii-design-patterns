package lab.patterns.structural.proxy.image;

public class LaunchImage {

    public static void main(String[] args) {
        System.out.println("let's display some images");

        Image i1 = new ProxyImage("i1.png");
        Image i2 = new ProxyImage("i2.png");

        i2.display();
    }
}
