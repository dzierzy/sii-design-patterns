package lab.patterns.structural.proxy.image;

public class ProxyImage implements Image {

    private String imageFileName;

    public ProxyImage(String imageFileName){
        this.imageFileName = imageFileName;
    }

    @Override
    public void display() {
        Image i = new RealImage(imageFileName);
        i.display();
    }
}
