package lab.patterns.structural.proxy.image;

public interface Image {

    void display();

}
