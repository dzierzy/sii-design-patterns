package lab.patterns.structural.adapter.sim;

public class LaunchDevice {

    public static void main(String[] args) {
        System.out.println("let's install sim card");

        Device d = new Device();

        MiniSimCard miniSim = new MiniSimCard("625472542364");

        Sim sim = new MiniSim2SimAdapter(miniSim);
        d.install(sim);
    }
}
