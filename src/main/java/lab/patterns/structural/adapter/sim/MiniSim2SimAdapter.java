package lab.patterns.structural.adapter.sim;

public class MiniSim2SimAdapter implements Sim {

    private MiniSimCard miniSimCard;

    public MiniSim2SimAdapter(MiniSimCard miniSimCard) {
        this.miniSimCard = miniSimCard;
    }

    @Override
    public String getId() {
        return new String(this.miniSimCard.getId());
    }
}
