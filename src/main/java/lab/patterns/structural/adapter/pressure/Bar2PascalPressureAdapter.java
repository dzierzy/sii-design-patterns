package lab.patterns.structural.adapter.pressure;

import com.sun.javafx.image.impl.ByteBgraPre;
import lab.patterns.structural.adapter.pressure.bar.BarPressureProvider;

public class Bar2PascalPressureAdapter implements PascalPressure {


    BarPressureProvider barPressure = new BarPressureProvider();

    @Override
    public float getPressure() {
        return barPressure.getPressure()*100000;
    }
}
