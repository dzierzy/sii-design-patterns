package lab.patterns.structural.adapter.pressure;

public interface PascalPressure {

    float getPressure();
}
