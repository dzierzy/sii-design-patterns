package lab.patterns.structural.adapter.pressure;

public class LaunchPressure {

    public static void main(String[] args) {
        System.out.println("let's measure a pressure !");

        PascalPressure pp = new Bar2PascalPressureAdapter();

        Display d = new Display();
        d.showPresure(pp);

    }
}
