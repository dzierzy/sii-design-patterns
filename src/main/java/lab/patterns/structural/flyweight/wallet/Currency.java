package lab.patterns.structural.flyweight.wallet;

public enum Currency {

    PLN,
    EURO,
    DOLLAR
}
