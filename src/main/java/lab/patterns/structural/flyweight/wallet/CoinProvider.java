package lab.patterns.structural.flyweight.wallet;

import java.util.HashMap;
import java.util.Map;

public class CoinProvider {

    private static Map<String, Coin> coins = new HashMap<>();

    public static Coin getCoin(int value, Currency currency){

        Coin coin = coins.get(value + currency.name());
        if(coin==null){
            coin = new Coin(value, currency);
            coins.put(value+currency.name(),coin);
        }
        return coin;
    }
}
