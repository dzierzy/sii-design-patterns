package lab.patterns.structural.flyweight.wallet;

import java.util.HashMap;
import java.util.Map;

public class CoinCache {

    private static Map<Currency, Map<Integer, Coin>> cache = new HashMap<>();

    public static Coin getCoin(int value, Currency currency){ // 2 euro

        Map<Integer, Coin> map = cache.get(currency); // euro
        Coin coin = null;
        if(map==null) {
            map = new HashMap<>();
            cache.put(currency, map);
        } else {
            coin = map.get(value); // 2
        }

        if(coin==null){
            coin = new Coin(value, currency);
            map.put(value, coin);
        }
        return coin;
    }

}
