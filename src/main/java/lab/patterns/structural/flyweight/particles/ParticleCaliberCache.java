package lab.patterns.structural.flyweight.particles;

import java.util.HashMap;
import java.util.Map;

public class ParticleCaliberCache {

    private static Map<Integer, ParticleCaliber> cache = new HashMap<>();

    public static ParticleCaliber getParticleCaliber(int caliber){

        ParticleCaliber pc = cache.get(caliber);
        if(pc==null){
            pc = new ParticleCaliber(caliber);
            cache.put(caliber, pc);
        }
        return pc;

    }

}
