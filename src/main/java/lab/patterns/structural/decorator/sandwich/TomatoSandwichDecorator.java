package lab.patterns.structural.decorator.sandwich;

public class TomatoSandwichDecorator extends SandwichDecorator {

    public TomatoSandwichDecorator(Sandwich delegate) {
        super(delegate);
    }

    @Override
    public String getDecoration() {
        return "tomato";
    }
}
