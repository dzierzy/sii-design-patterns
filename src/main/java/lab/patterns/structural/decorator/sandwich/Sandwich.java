package lab.patterns.structural.decorator.sandwich;

public interface Sandwich {

    String content();
}
