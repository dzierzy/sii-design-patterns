package lab.patterns.structural.decorator.sandwich;

public class LauchSandwich {

    public static void main(String[] args) {
        System.out.println("let's eat some sandwich");

        Sandwich sandwich = new CheeseSandwichDecorator(new TomatoSandwichDecorator(new SalamiSandwichDecorator(new WhiteBreadSandwich())));

        System.out.println("eating " + sandwich.content());
    }
}
