package lab.patterns.structural.decorator.sandwich;

public class KetchupSandwichDecorator implements Sandwich{

    private Sandwich delegate;

    public KetchupSandwichDecorator(Sandwich delegate) {
        this.delegate = delegate;
    }

    @Override
    public String content() {
        return "ketchup, " + delegate.content();
    }
}
