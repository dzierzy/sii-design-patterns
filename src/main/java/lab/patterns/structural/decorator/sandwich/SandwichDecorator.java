package lab.patterns.structural.decorator.sandwich;

public abstract class SandwichDecorator implements Sandwich {


    private Sandwich delegate;

    public SandwichDecorator(Sandwich delegate) {
        this.delegate = delegate;
    }

    @Override
    public String content() {
        return getDecoration() + ", " + delegate.content();
    }

    public abstract String getDecoration();
}
