package lab.patterns.structural.decorator.sandwich;

public class WhiteBreadSandwich implements Sandwich {
    @Override
    public String content() {
        return "white bread";
    }
}
