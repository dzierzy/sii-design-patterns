package lab.patterns.structural.decorator.sandwich;

public class CheeseSandwichDecorator extends SandwichDecorator {


    public CheeseSandwichDecorator(Sandwich delegate) {
        super(delegate);
    }

    @Override
    public String getDecoration() {
        return "cheese";
    }
}
