package lab.patterns.structural.decorator.sandwich;

public class SalamiSandwichDecorator extends SandwichDecorator {

    public SalamiSandwichDecorator(Sandwich delegate) {
        super(delegate);
    }

    @Override
    public String getDecoration() {
        return "salami";
    }
}
