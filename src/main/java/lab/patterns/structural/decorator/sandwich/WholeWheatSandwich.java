package lab.patterns.structural.decorator.sandwich;

public class WholeWheatSandwich implements Sandwich {
    @Override
    public String content() {
        return "whole wheat sandwich";
    }
}
