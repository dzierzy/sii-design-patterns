package lab.patterns.structural.decorator.email;


public interface Email {

    String getTitle();

    String getContent();
}
