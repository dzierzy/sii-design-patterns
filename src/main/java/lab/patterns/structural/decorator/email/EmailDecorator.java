package lab.patterns.structural.decorator.email;

public abstract class EmailDecorator implements Email {

    Email email;

    public EmailDecorator(Email email) {
        this.email = email;
    }

    @Override
    public final String getTitle() {
        return email.getTitle();
    }

    @Override
    public String getContent() {
        return email.getContent() + "\n" + getSignature();
    }


    protected abstract String getSignature();
}
