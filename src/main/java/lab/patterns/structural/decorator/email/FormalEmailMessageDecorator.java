package lab.patterns.structural.decorator.email;

public class FormalEmailMessageDecorator implements Email {

    private Email delegate;

    public FormalEmailMessageDecorator(Email delegate)
    {
        this.delegate = delegate;
    }

    @Override
    public String getTitle()
    {
        return delegate.getTitle();
    }

    @Override
    public String getContent()
    {
        return delegate.getContent() + "\n Best Regards! John Doe.";
    }
}
