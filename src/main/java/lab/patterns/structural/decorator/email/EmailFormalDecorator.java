package lab.patterns.structural.decorator.email;

public class EmailFormalDecorator extends EmailDecorator {


    public EmailFormalDecorator(Email email) {
        super(email);
    }

    @Override
    protected String getSignature() {
        return "Regards, \n Joe Doe";
    }
}
