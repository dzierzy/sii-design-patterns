package lab.patterns.structural.decorator.email;

public class LaunchEmail {


    public static void main(String[] args) {
        System.out.println("let's send some email");

        Email email = new EmailInformalDecorator(new EmailMessage("Hey Joe", "How are you doing?"));

        System.out.println("sending email: title=[" + email.getTitle() + "], content=[" + email.getContent() + "]");

    }

}
