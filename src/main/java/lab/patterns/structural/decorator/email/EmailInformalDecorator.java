package lab.patterns.structural.decorator.email;

public class EmailInformalDecorator extends EmailDecorator {

    public EmailInformalDecorator(Email email) {
        super(email);
    }

    @Override
    protected String getSignature() {
        return "Cheers!\nJoe";
    }
}
