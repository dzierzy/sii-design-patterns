package lab.patterns.creational.builder.pizza;

public class LauchPizza {

    public static void main(String[] args) {
        System.out.println("let's have some pizza!");

        Pizza p = new PizzaBuilder()
                .addDough("cienkie")
                .addTopping("ser, pieczarki")
                .addSause("czosnkowy")
                .bake();

        System.out.println("eating pizza: " + p);
    }
}
