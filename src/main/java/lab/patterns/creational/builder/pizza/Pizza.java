package lab.patterns.creational.builder.pizza;

public class Pizza {

    private String dough;

    private String topping;

    private String sause;

    public String getDough() {
        return dough;
    }

    public void setDough(String dough) {
        this.dough = dough;
    }

    public String getTopping() {
        return topping;
    }

    public void setTopping(String topping) {
        this.topping = topping;
    }

    public String getSause() {
        return sause;
    }

    public void setSause(String sause) {
        this.sause = sause;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "dough='" + dough + '\'' +
                ", topping='" + topping + '\'' +
                ", sause='" + sause + '\'' +
                '}';
    }
}
