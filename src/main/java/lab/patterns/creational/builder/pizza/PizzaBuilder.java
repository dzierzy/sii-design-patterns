package lab.patterns.creational.builder.pizza;

public class PizzaBuilder {

    private String dough;

    private String topping;

    private String sause;

    PizzaBuilder addDough(String dough){
        this.dough = dough;
        return this;
    }

    PizzaBuilder addTopping(String topping){
        this.topping = topping;
        return this;
    }

    PizzaBuilder addSause(String sause){
        this.sause = sause;
        return this;
    }

    Pizza bake(){
        Pizza pizza = new Pizza();
        if(dough==null) throw new IllegalArgumentException("dough is mandatory");
        pizza.setDough(dough);
        pizza.setTopping(topping);
        pizza.setSause(sause);
        return pizza;
    }

}
