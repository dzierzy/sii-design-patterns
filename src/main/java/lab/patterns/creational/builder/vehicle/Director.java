package lab.patterns.creational.builder.vehicle;

public class Director {

    private VehicleBuilder vehicleBuilder;

    public void setVehicleBuilder(VehicleBuilder car) {
        vehicleBuilder = car;
    }

    public Vehicle constructAuto() {
        vehicleBuilder.createNewAutoProduct();
        vehicleBuilder.buildColor();
        vehicleBuilder.buildEngine();
        vehicleBuilder.buildWheels();
        return vehicleBuilder.getConstructedProduct();
    }
}

