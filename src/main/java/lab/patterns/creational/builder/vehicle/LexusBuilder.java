package lab.patterns.creational.builder.vehicle;

public class LexusBuilder extends VehicleBuilder {
    @Override
    public void buildEngine() {
        this.auto.setEngine("5.0");
    }

    @Override
    public void buildColor() {
        this.auto.setColor("gold");
    }

    @Override
    public void buildWheels() {
        this.auto.setWheels("alu");
    }
}
