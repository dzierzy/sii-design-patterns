package lab.patterns.creational.builder.vehicle;

public class LaunchVehicle {

	public static void main(String[] args) {

		Director director = new Director();
		VehicleBuilder chevroletBuilder = new LexusBuilder();
		
		director.setVehicleBuilder(chevroletBuilder);
		Vehicle auto = director.constructAuto();

		//Vehicle auto = chevroletBuilder.getConstructedProduct();

		auto.drive();


	}

}
