package lab.patterns.creational.builder.vehicle;

public abstract class VehicleBuilder {

    protected Vehicle auto;

    public void createNewAutoProduct() {
        auto = new Vehicle();
    }

    public abstract void buildEngine();

    public abstract void buildColor();

    public void buildWheels(){
        auto.setWheels("steel");
    }

    public Vehicle getConstructedProduct(){
        return auto;
    }
}

