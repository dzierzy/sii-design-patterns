package lab.patterns.creational.builder.vehicle;

public class Vehicle {

	private String engine = "";
	private String color = "";

	private String wheels;

	public void setEngine(String engine)
	{
		this.engine = engine; 
		System.out.println(this.engine);
	}

	public void setColor(String color)
	{
		this.color = color; 
		System.out.println(this.color);
	}

	public void setWheels(String wheels) {
		this.wheels = wheels;
	}

	public void drive(){
		System.out.println("rrr...");
	}
}
