package lab.patterns.creational.builder.vehicle;

public class ChevroletBuilder extends VehicleBuilder {
	public void buildEngine() 
	{
		auto.setEngine("1.4");
	}
	public void buildColor() 
	{
		auto.setColor("red");
		}
}
