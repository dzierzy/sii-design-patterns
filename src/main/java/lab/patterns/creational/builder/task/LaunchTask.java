package lab.patterns.creational.builder.task;

import java.util.Locale;

public class LaunchTask {

    public static void main(String[] args) {
        System.out.println("let's register a task");

        Task t = new TaskBuilder()
                .addId(1)
                //.addTitle("first feature")
                .addDescription("Lorem ipsum")
                .addPriority(1)
                .build();

                /*new Task();
        t.setTitle("first feature");
        t.setDescription("Lorem ipsum ......");
        t.setId(1);
        t.setPriority(1);*/

        System.out.println("task created: " + t);


        String s = new StringBuilder().append(1).append("smth").toString();

        Locale locale = new Locale.Builder().setLanguage("pl").setRegion("PL").build();

    }
}
