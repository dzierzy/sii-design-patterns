package lab.patterns.creational.builder.task;

public class TaskBuilder {

    private String title;

    private String description;

    private int priority;

    private int id;

    TaskBuilder addTitle(String title){
        this.title = title;
        return this;
    }

    TaskBuilder addDescription(String description){
        this.description = description;
        return this;
    }

    TaskBuilder addId(int id){
        this.id = id;
        return this;
    }

    TaskBuilder addPriority(int priority){
        if(priority<1){
            throw new IllegalArgumentException("priority cen not be negative");
        }
        this.priority = priority;
        return this;
    }




    Task build(){
        Task t = new Task();
        t.setId(id);
        if(title==null){
            throw new IllegalArgumentException("title is mandatory");
        }
        t.setTitle(title);
        t.setDescription(description);
        t.setPriority(priority);
        return t;
    }



}
