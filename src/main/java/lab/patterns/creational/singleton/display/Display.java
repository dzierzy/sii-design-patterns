package lab.patterns.creational.singleton.display;

public enum Display {

    INSTANCE;

    public void display(String message){
        System.out.println("message: " + message);
    }

}
