package lab.patterns.creational.singleton.display;

import java.time.LocalDate;
import java.time.LocalTime;

public class LaunchDisplay {

    public static void main(String[] args) {
        System.out.println("let's display smth");
        Display d1 = Display.INSTANCE;
        d1.display(LocalDate.now().toString());
        Display d2 = Display.INSTANCE;
        d2.display(LocalTime.now().toString());
    }
}
