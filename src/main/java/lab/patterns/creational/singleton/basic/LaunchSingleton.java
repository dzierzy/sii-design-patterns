package lab.patterns.creational.singleton.basic;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class LaunchSingleton {

    public static void main(String[] args) {
        System.out.println("Let's test singleton");
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();

        Class clazz = Singleton.class;
        try {
            Constructor constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            s2 = (Singleton)constructor.newInstance();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        System.out.println(s1==s2);
    }
}
