package lab.patterns.creational.factory.method.beer;

public class Pilsner implements Beer {
    @Override
    public float getVoltage() {
        return 4.5f;
    }

    @Override
    public String getName() {
        return "Pilsner";
    }
}
