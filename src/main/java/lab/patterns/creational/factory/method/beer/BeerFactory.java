package lab.patterns.creational.factory.method.beer;

public abstract class BeerFactory  {

    public abstract Beer getBeer(BeerType beerType);

}
