package lab.patterns.creational.factory.method.beer.craft;

import lab.patterns.creational.factory.method.beer.*;

public class CraftBeerFactory extends BeerFactory {
    @Override
    public Beer getBeer(BeerType beerType) {
        switch (beerType) {
            case PILS:
                return new KoniecSwiata();
            case IPA:
                return new OtoMataIpa();
            default:
                return new KoniecSwiata();
        }
    }
}
