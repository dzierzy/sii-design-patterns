package lab.patterns.creational.factory.method.vehicle;

public class AstonMartin implements Car {
	public void show()
	{
		System.out.println("AstonMartin Car!");
	}
}
