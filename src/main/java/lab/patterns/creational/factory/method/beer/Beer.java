package lab.patterns.creational.factory.method.beer;

public interface Beer {

    float getVoltage();

    String getName();

}
