package lab.patterns.creational.factory.method.vehicle;

public class LandRover implements Car {
	public void show()
	{
		System.out.println("LandRover Car!");
	}
}

