package lab.patterns.creational.factory.method.beer.craft;

import lab.patterns.creational.factory.method.beer.Beer;

public class KoniecSwiata implements Beer {
    @Override
    public float getVoltage() {
        return 10;
    }

    @Override
    public String getName() {
        return "Koniec swiata";
    }
}
