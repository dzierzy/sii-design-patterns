package lab.patterns.creational.factory.method.beer;

public class WisniaBeer implements Beer {
    @Override
    public float getVoltage() {
        return 1.5f;
    }

    @Override
    public String getName() {
        return "Kriek";
    }
}
