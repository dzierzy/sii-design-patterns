package lab.patterns.creational.factory.method.beer;

public enum BeerType {
    PILS,
    IPA,
    FRUITY
}
