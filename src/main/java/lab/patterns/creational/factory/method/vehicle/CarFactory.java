package lab.patterns.creational.factory.method.vehicle;

public class CarFactory {

    public static Car getCar(CarType type){
        if(type==CarType.SUV){
            return new LandRover();
        } else if(type==CarType.LIMOUSINE){
            return new AstonMartin();
        } else return null;
    }
}
