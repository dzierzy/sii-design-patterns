package lab.patterns.creational.factory.method.beer.craft;

import lab.patterns.creational.factory.method.beer.Beer;

public class OtoMataIpa implements Beer {
    @Override
    public float getVoltage() {
        return 7;
    }

    @Override
    public String getName() {
        return "oto mata";
    }
}
