package lab.patterns.creational.factory.method.vehicle;

public class LaunchCar {

	public static void main(String[] args) {

		// limousine
		Car vehicle = CarFactory.getCar(CarType.LIMOUSINE);
		vehicle.show();

		// suv
		vehicle = CarFactory.getCar(CarType.SUV);
		vehicle.show();
	}

}


