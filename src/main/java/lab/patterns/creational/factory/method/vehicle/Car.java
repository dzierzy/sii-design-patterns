package lab.patterns.creational.factory.method.vehicle;

public interface Car {
	public void show();
}
