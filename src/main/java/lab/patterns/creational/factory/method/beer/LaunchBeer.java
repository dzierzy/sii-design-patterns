package lab.patterns.creational.factory.method.beer;

import lab.patterns.creational.factory.method.beer.craft.CraftBeerFactory;

public class LaunchBeer {

    public static void main(String[] args) {
        System.out.println("let's have some beer!");


        BeerFactory bf = new CraftBeerFactory();
        Beer b = bf.getBeer(BeerType.IPA);

        System.out.println("drinking " + b.getName());
    }
}
