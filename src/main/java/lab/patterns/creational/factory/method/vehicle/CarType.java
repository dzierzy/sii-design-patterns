package lab.patterns.creational.factory.method.vehicle;

public enum CarType {

    SUV,
    CROSSOVER,
    LIMOUSINE

}
