package lab.patterns.creational.factory.method.beer;

public class SimpleBeerFactory extends BeerFactory{


    public Beer getBeer(BeerType beerType) {
        return new Pilsner();
    }
}
