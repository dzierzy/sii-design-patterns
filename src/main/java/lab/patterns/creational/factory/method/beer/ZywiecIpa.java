package lab.patterns.creational.factory.method.beer;

public class ZywiecIpa implements  Beer {

    @Override
    public float getVoltage() {
        return 6;
    }

    @Override
    public String getName() {
        return "zywiec ipa";
    }
}
