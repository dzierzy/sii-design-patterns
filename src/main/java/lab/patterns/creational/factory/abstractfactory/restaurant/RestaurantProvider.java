package lab.patterns.creational.factory.abstractfactory.restaurant;

import lab.patterns.creational.factory.abstractfactory.restaurant.italian.ItalianRestaurantFactory;
import lab.patterns.creational.factory.abstractfactory.restaurant.mcdonalds.McDonalds;

public class RestaurantProvider {
    public static Restaurant getRestaurant(RestaurantType restaurant) {
        switch (restaurant){
            case MCDONALDS: return new McDonalds();
            case ITALIAN: return new ItalianRestaurantFactory();
            default:return null;
        }
    }
}
