package lab.patterns.creational.factory.abstractfactory.restaurant;

public class LaunchRestaurant {

    public static void main(String[] args) {


        Restaurant rf = RestaurantProvider.getRestaurant(RestaurantType.ITALIAN);

        Appetizer appetizer = rf.getAppetizer();
        MainDish mainDish = rf.getMainDish();
        Dessert dessert = rf.getDessert();

        appetizer.taste();
        mainDish.eat();
        dessert.enjoy();
    }
}
