package lab.patterns.creational.factory.abstractfactory.restaurant.italian;

import lab.patterns.creational.factory.abstractfactory.restaurant.Appetizer;
import lab.patterns.creational.factory.abstractfactory.restaurant.Soup;

public class Minestrone implements Appetizer {
    @Override
    public void taste() {
        System.out.println("Drinking italian minestrone soup...");
    }
}
