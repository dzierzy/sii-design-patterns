package lab.patterns.creational.factory.abstractfactory.animals.predators;

import lab.patterns.creational.factory.abstractfactory.animals.AnimalFactory;
import lab.patterns.creational.factory.abstractfactory.animals.Bird;
import lab.patterns.creational.factory.abstractfactory.animals.Fish;
import lab.patterns.creational.factory.abstractfactory.animals.Mammal;

public class PredatorsAnimalFactory implements AnimalFactory {
    @Override
    public Bird getBird() {
        return new Eagle();
    }

    @Override
    public Fish getFish() {
        return new Shark();
    }

    @Override
    public Mammal getMammal() {
        return new Tiger();
    }
}
