package lab.patterns.creational.factory.abstractfactory.animals;

public interface Bird {

    void fly();
}
