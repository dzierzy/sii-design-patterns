package lab.patterns.creational.factory.abstractfactory.animals.vege;

import lab.patterns.creational.factory.abstractfactory.animals.Mammal;

public class Elephant implements Mammal {
    @Override
    public void move() {
        System.out.println("elephant is walking....");
    }
}
