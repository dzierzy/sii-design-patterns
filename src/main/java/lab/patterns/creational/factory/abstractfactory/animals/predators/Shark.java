package lab.patterns.creational.factory.abstractfactory.animals.predators;

import lab.patterns.creational.factory.abstractfactory.animals.Fish;

public class Shark implements Fish {
    @Override
    public void swim() {
        System.out.println("shark is swimming");
    }
}
