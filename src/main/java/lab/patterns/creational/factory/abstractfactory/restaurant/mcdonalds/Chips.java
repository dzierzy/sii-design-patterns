package lab.patterns.creational.factory.abstractfactory.restaurant.mcdonalds;

import lab.patterns.creational.factory.abstractfactory.restaurant.Appetizer;

public class Chips implements Appetizer {

    @Override
    public void taste() {
        System.out.println("Taste your Chips!");
    }
}
