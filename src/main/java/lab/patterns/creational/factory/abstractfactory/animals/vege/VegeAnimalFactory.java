package lab.patterns.creational.factory.abstractfactory.animals.vege;

import lab.patterns.creational.factory.abstractfactory.animals.AnimalFactory;
import lab.patterns.creational.factory.abstractfactory.animals.Bird;
import lab.patterns.creational.factory.abstractfactory.animals.Fish;
import lab.patterns.creational.factory.abstractfactory.animals.Mammal;

public class VegeAnimalFactory implements AnimalFactory {
    @Override
    public Bird getBird() {
        return new Canary();
    }

    @Override
    public Fish getFish() {
        return new GoldFish();
    }

    @Override
    public Mammal getMammal() {
        return new Elephant();
    }
}
