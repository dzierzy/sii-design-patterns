package lab.patterns.creational.factory.abstractfactory.restaurant;

public interface Dessert {

    void enjoy();
}
