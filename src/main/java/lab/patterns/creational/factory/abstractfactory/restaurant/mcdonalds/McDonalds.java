package lab.patterns.creational.factory.abstractfactory.restaurant.mcdonalds;

import lab.patterns.creational.factory.abstractfactory.restaurant.Appetizer;
import lab.patterns.creational.factory.abstractfactory.restaurant.Dessert;
import lab.patterns.creational.factory.abstractfactory.restaurant.MainDish;
import lab.patterns.creational.factory.abstractfactory.restaurant.Restaurant;

public class McDonalds implements Restaurant {
    @Override
    public Appetizer getAppetizer() {
        return new Chips();
    }

    @Override
    public MainDish getMainDish() {
        return new BigMac();
    }

    @Override
    public Dessert getDessert() {
        return new McFlurry();
    }
}
