package lab.patterns.creational.factory.abstractfactory.animals;

public enum AnimalType {

    VEGE,
    PREDATOR
}
