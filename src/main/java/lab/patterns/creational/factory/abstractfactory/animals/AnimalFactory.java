package lab.patterns.creational.factory.abstractfactory.animals;

public interface AnimalFactory {

    Bird getBird();

    Fish getFish();

    Mammal getMammal();

}
