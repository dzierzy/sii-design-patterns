package lab.patterns.creational.factory.abstractfactory.animals.vege;

import lab.patterns.creational.factory.abstractfactory.animals.Bird;

public class Canary implements Bird {
    @Override
    public void fly() {
        System.out.println("canary is flying");
    }
}
