package lab.patterns.creational.factory.abstractfactory.restaurant.italian;

import lab.patterns.creational.factory.abstractfactory.restaurant.Dessert;

public class IceCream implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("Enjoy you italian ice cream dessert...");
    }
}
