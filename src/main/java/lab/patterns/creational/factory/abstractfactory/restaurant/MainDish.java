package lab.patterns.creational.factory.abstractfactory.restaurant;

public interface MainDish {

    void eat();
}
