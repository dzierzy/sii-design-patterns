package lab.patterns.creational.factory.abstractfactory.animals;

import lab.patterns.creational.factory.abstractfactory.animals.predators.PredatorsAnimalFactory;
import lab.patterns.creational.factory.abstractfactory.animals.vege.VegeAnimalFactory;

public class AnimalFactoryProvider {

    public static AnimalFactory getAnimalFactory(AnimalType at){

        switch (at){
            case VEGE: return new VegeAnimalFactory();
            case PREDATOR:return new PredatorsAnimalFactory();
            default:return null;
        }
    }
}
