package lab.patterns.creational.factory.abstractfactory.restaurant;

public enum RestaurantType {

    MCDONALDS,
    POLISH,
    ITALIAN
}
