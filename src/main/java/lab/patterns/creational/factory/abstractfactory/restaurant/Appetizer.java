package lab.patterns.creational.factory.abstractfactory.restaurant;

public interface Appetizer {

    void taste();
}
