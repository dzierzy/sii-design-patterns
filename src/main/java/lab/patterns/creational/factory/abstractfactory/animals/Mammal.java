package lab.patterns.creational.factory.abstractfactory.animals;

public interface Mammal {

    void move();
}
