package lab.patterns.creational.factory.abstractfactory.restaurant;

public interface Soup {

    void drink();
}
