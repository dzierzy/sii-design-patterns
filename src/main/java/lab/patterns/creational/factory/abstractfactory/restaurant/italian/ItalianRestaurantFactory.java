package lab.patterns.creational.factory.abstractfactory.restaurant.italian;

import lab.patterns.creational.factory.abstractfactory.restaurant.*;

public class ItalianRestaurantFactory implements Restaurant {

    @Override
    public Appetizer getAppetizer() {
        return new Minestrone();
    }

    @Override
    public MainDish getMainDish() {
        return new Pasta();
    }

    @Override
    public Dessert getDessert() {
        return new IceCream();
    }
}
