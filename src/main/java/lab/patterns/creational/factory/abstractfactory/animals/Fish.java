package lab.patterns.creational.factory.abstractfactory.animals;

public interface Fish {

    void swim();

}
