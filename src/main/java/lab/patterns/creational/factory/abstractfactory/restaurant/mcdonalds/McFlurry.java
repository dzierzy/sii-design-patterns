package lab.patterns.creational.factory.abstractfactory.restaurant.mcdonalds;

import lab.patterns.creational.factory.abstractfactory.restaurant.Dessert;

public class McFlurry implements Dessert {
    @Override
    public void enjoy() {
        System.out.println("Enjoy those new McFlurry ice creams!");
    }
}
