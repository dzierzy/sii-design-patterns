package lab.patterns.creational.factory.abstractfactory.animals.predators;

import lab.patterns.creational.factory.abstractfactory.animals.Bird;

public class Eagle implements Bird {
    @Override
    public void fly() {
        System.out.println("eagle is flying");
    }
}
