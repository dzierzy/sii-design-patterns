package lab.patterns.creational.factory.abstractfactory.restaurant.italian;

import lab.patterns.creational.factory.abstractfactory.restaurant.MainDish;

public class Pasta implements MainDish {
    @Override
    public void eat() {
        System.out.println("Eating italian pasta...");
    }
}
