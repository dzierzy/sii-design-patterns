package lab.patterns.creational.factory.abstractfactory.animals.vege;

import lab.patterns.creational.factory.abstractfactory.animals.Fish;

public class GoldFish implements Fish {
    @Override
    public void swim() {
        System.out.println("gold fish isawimming and making your dreams come true!");
    }
}
