package lab.patterns.creational.factory.abstractfactory.animals;

import lab.patterns.creational.factory.abstractfactory.animals.predators.PredatorsAnimalFactory;
import lab.patterns.creational.factory.abstractfactory.animals.vege.VegeAnimalFactory;

public class LaunchAnimal {

    public static void main(String[] args) {

        AnimalFactory af = AnimalFactoryProvider.getAnimalFactory(AnimalType.PREDATOR);

        Bird bird = af.getBird();
        Fish fish = af.getFish();
        Mammal mammal = af.getMammal();

        bird.fly();
        fish.swim();
        mammal.move();

    }
}
