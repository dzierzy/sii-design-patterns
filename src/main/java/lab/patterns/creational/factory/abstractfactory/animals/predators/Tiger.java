package lab.patterns.creational.factory.abstractfactory.animals.predators;

import lab.patterns.creational.factory.abstractfactory.animals.Mammal;

public class Tiger implements Mammal {
    @Override
    public void move() {
        System.out.println("tiger is running!");
    }
}
