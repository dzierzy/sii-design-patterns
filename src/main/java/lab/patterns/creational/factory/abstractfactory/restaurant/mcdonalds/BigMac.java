package lab.patterns.creational.factory.abstractfactory.restaurant.mcdonalds;

import lab.patterns.creational.factory.abstractfactory.restaurant.MainDish;

public class BigMac implements MainDish {
    @Override
    public void eat() {
        System.out.println("Eat your BigMac");
    }
}
