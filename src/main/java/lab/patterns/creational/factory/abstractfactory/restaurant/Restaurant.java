package lab.patterns.creational.factory.abstractfactory.restaurant;

public interface Restaurant {

    Appetizer getAppetizer();

    MainDish getMainDish();

    Dessert getDessert();
}
