package lab.patterns.behavioral.observer.followers;

import java.util.ArrayList;
import java.util.List;

public class Profile {

    private List<Follower> followers = new ArrayList<>();

    public void registerFollower(Follower follower){
        followers.add(follower);
    }

    public void publish(String message) {
        Post p = new Post(message);
        System.out.println("post generated: " + p.getMessage());
        broadcast(p);
    }

    private void broadcast(Post p){
        for(Follower listener : followers){
            listener.inform(p);
        }
    }

}
