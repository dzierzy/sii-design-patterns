package lab.patterns.behavioral.observer.followers;

public class Post {

    private String message;

    public Post(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
