package lab.patterns.behavioral.observer.followers;

public class YTFollower implements Follower {
    @Override
    public void inform(Post post)
    {
        System.out.println("Dear YouTube Users, " + post.getMessage());
    }
}
