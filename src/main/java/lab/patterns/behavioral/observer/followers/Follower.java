package lab.patterns.behavioral.observer.followers;

public interface Follower {

    void inform(Post post);

}
