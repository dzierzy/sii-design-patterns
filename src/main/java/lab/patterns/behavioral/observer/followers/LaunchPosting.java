package lab.patterns.behavioral.observer.followers;

public class LaunchPosting {

    public static void main(String[] args) {
        Profile profile = new Profile();

        profile.registerFollower(new FBFollower());
        profile.registerFollower(new YTFollower());

        profile.publish("totally unimportant message 1");
        profile.publish("totally unimportant message 2");
    }
}
