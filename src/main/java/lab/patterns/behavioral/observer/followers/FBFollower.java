package lab.patterns.behavioral.observer.followers;

public class FBFollower implements Follower {
    @Override
    public void inform(Post post)
    {
        System.out.println("Dear FaceBook Users, " + post.getMessage());
    }
}
