package lab.patterns.behavioral.observer.press;

public class LaunchNews {

    public static void main(String[] args) {

        PressOffice po = new PressOffice();
        po.registerNewsListener(n-> System.out.println("ad hoc listener: " + n));
        po.registerNewsListener(new SMSListener());
        po.registerNewsListener(new RSSListener());

        po.dayWork();

    }
}
