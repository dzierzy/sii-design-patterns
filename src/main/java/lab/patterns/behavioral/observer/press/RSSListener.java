package lab.patterns.behavioral.observer.press;

public class RSSListener implements NewsListener {
    @Override
    public void receiveNews(News n) {
        System.out.println("RSS: " + n);
    }
}
