package lab.patterns.behavioral.observer.press;

import java.util.ArrayList;
import java.util.List;

public class PressOffice {

    private List<NewsListener> listeners = new ArrayList<>();

    public void dayWork(){

        News n1 = new News("Mundial: Poland won!", "Polska mistrzem Polski");
        broadast(n1);

        News n2 = new News("Chemitrails are fake", "Chemitrails doen't exists");
        broadast(n2);

    }

    public void registerNewsListener(NewsListener listener){
        listeners.add(listener);
    }


    private void broadast(News n){
        for(NewsListener listener : listeners){
            listener.receiveNews(n);
        }
    }


}
