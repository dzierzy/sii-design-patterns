package lab.patterns.behavioral.observer.press;

public class SMSListener implements NewsListener {
    @Override
    public void receiveNews(News n) {
        System.out.println("SMS: " + n);
    }
}
