package lab.patterns.behavioral.observer.press;

public interface NewsListener {


    void receiveNews(News n);

}
