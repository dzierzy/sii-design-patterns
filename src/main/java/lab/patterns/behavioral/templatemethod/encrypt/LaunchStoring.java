package lab.patterns.behavioral.templatemethod.encrypt;


public class LaunchStoring {

    public static void main(String[] args){
        SafeStore store = new DESStore();
        boolean stored = store.store("hide it!");
        System.out.println("safely stored? " + stored);
    }

}
