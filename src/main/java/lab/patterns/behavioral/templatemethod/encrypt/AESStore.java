package lab.patterns.behavioral.templatemethod.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;


public class AESStore extends SafeStore {

    private static final String ALG = "AES";

    private static final byte[] KEY = new byte[]{'v', 'e', 'r', 'y','s','e','c','r','e','t','v', 'e', 'r', 'y','s','e'};

    protected byte[] encrypt(String s) throws Exception {
        Key key = new SecretKeySpec(KEY, ALG);
        Cipher c = Cipher.getInstance(ALG);
        c.init(Cipher.ENCRYPT_MODE, key);
        return c.doFinal(s.getBytes());
    }
}
