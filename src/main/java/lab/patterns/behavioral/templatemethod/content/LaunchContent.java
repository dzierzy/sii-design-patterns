package lab.patterns.behavioral.templatemethod.content;

public class LaunchContent {


    public static void main(String[] args) {
        System.out.println("let's represent some content");

        ContentRepresentation content = new XmlRepresentation("some content");

        System.out.println(content.representContent());
        // <content>text</content>
        // { content: text }

    }
}
