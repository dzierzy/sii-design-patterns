package lab.patterns.behavioral.templatemethod.content;

public abstract class ContentRepresentation {

    protected String content;

    public ContentRepresentation(String content) {
        this.content = content;
    }

    protected final String representContent(){
        return getPrefix() + content + getSuffix();
    }

    protected abstract String getSuffix();

    protected abstract String getPrefix();


}
