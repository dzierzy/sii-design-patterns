package lab.patterns.behavioral.templatemethod.content;

public class XmlRepresentation extends ContentRepresentation {

    public XmlRepresentation(String content) {
        super(content);
    }


    @Override
    protected String getSuffix() {
        return "</content>";
    }

    @Override
    protected String getPrefix() {
        return "<content>";
    }
}
