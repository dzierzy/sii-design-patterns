package lab.patterns.behavioral.templatemethod.content;

public class JsonRepresentation extends ContentRepresentation {

    public JsonRepresentation(String content) {
        super(content);
    }

    @Override
    protected String getSuffix() {
        return "}";
    }

    @Override
    protected String getPrefix() {
        return "{ content: ";
    }
}
