package lab.patterns.behavioral.strategy.payment;

/**
 * Created by xdzm on 2016-06-15.
 */
public class Item {

    private String name;

    private int amount;

    private double price;


    public Item(String name, int amount, double price) {
        this.name = name;
        this.amount = amount;
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public double getPrice() {
        return price;
    }
}
