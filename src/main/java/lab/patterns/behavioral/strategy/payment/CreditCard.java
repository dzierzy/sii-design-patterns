package lab.patterns.behavioral.strategy.payment;

public class CreditCard implements PaymentMethod {


    private String number;

    private int ccv;


    public CreditCard() {
    }

    public CreditCard(String number, int ccv) {
        this.number = number;
        this.ccv = ccv;
    }

    @Override
    public void charge(double value) {
        System.out.println("charching card " + number);
    }
}
