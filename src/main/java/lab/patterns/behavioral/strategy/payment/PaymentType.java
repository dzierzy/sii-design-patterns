package lab.patterns.behavioral.strategy.payment;

public enum PaymentType {

    PAYPAL,
    CREDIT_CARD
}
