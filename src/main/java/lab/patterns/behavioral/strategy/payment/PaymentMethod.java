package lab.patterns.behavioral.strategy.payment;

public interface PaymentMethod {


    void charge(double value);
}
