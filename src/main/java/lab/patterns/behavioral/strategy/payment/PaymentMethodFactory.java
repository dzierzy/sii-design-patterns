package lab.patterns.behavioral.strategy.payment;

import java.util.Properties;

public class PaymentMethodFactory {

    public static PaymentMethod getPaymentMethod(PaymentType pt){
        switch(pt){
            case PAYPAL: return new Paypal();
            case CREDIT_CARD: return new CreditCard();
        }
        return null;
    }
}
