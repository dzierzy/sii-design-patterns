package lab.patterns.behavioral.strategy.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;


public class DESStore implements Encryption {

    private static final String ALG = "DES";

    private static final byte[] KEY = new byte[]{'v', 'e', 'r', 'y','s','e','c', 'r'};

    public byte[] encrypt (String s) throws Exception {
        Key key = new SecretKeySpec(KEY, ALG);
        Cipher c = Cipher.getInstance(ALG);
        c.init(Cipher.ENCRYPT_MODE, key);
        return c.doFinal(s.getBytes());
    }
}
