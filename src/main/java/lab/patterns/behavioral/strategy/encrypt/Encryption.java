package lab.patterns.behavioral.strategy.encrypt;

public interface Encryption {

    byte[] encrypt(String s) throws Exception;

}
