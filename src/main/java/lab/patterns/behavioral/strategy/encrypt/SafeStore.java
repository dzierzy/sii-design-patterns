package lab.patterns.behavioral.strategy.encrypt;


import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class SafeStore {


    public boolean store(Object o, Encryption encryption){
        try {
            byte[] toStore = encryption.encrypt(o.toString());
            storeEncrypted(toStore);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private final void storeEncrypted(byte[] bytes){

        try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("safe.store"))){
            bos.write(bytes);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
