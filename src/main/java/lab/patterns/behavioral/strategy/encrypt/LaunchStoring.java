package lab.patterns.behavioral.strategy.encrypt;


public class LaunchStoring {

    public static void main(String[] args){
        SafeStore store = new SafeStore();
        Encryption encryptionMethod = new AESStore();
        boolean stored = store.store("hide it!", encryptionMethod);
        System.out.println("safely stored? " + stored);
    }

}
