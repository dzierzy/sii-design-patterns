package lab.patterns.behavioral.cor.file;

import java.io.File;

public abstract class Parser {

    protected Parser next;

    public void setNext(Parser next) {
        this.next = next;
    }

    public void parse(File file){
        if(next!=null){
            next.parse(file);
        }
    }
}
