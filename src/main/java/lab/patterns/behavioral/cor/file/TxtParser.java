package lab.patterns.behavioral.cor.file;

import java.io.*;

public class TxtParser extends Parser {

    @Override
    public void parse(File file) {

        if(file.getName().endsWith(".txt")){
            try(BufferedReader br = new BufferedReader(new FileReader(file))){
                String line = null;
                while((line=br.readLine())!=null){
                    System.out.println(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            super.parse(file);
        }
    }
}
