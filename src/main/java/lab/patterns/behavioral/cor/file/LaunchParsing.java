package lab.patterns.behavioral.cor.file;

import java.io.File;

public class LaunchParsing {

    public static void main(String[] args) {
        System.out.println("let's parse a file");

        getChain().parse(new File("./src/main/resources/cor/toparse.csv"));
    }


    public static Parser getChain(){
        Parser txt = new TxtParser();
        Parser csv = new CsvParser();
        txt.setNext(csv);
        return txt;
    }
}
