package lab.patterns.behavioral.cor.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CsvParser extends Parser {

    @Override
    public void parse(File file) {

        if(file.getName().endsWith(".csv")){
            try(BufferedReader br = new BufferedReader(new FileReader(file))){
                String line = null;
                while((line=br.readLine())!=null){
                    String[] parsedLine = line.split(";");
                    for (String cell : parsedLine){
                        System.out.print("[" + cell + "]");
                    }
                    System.out.println();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if(next!=null){
                next.parse(file);
            }
        }
    }
}
