package lab.patterns.behavioral.cor.purchase;

public abstract class Approver {

    private Approver next;

    private String name;

    private int maxValue;

    public Approver(String name, int maxValue){
        this.name = name;
        this.maxValue = maxValue;
    }

    public void approve(Purchase p){
        if(p.getAmount() <= this.maxValue){
            System.out.println("Przyjęte do realizacji.");
            p.approve(name);
        } else{
            if(getNext()!=null){
                System.out.println("Zlecam to: " + next);
                getNext().approve(p);
            }
        }
    }

    public Approver getNext() {
        return next;
    }

    public Approver registerNext(Approver next) {
        this.next = next;
        return this;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Approver{" +
                "next=" + next +
                ", name='" + name + '\'' +
                ", maxValue=" + maxValue +
                '}';
    }
}
