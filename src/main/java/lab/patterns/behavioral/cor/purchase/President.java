package lab.patterns.behavioral.cor.purchase;

public class President extends Approver {

    public President(String name) {
        super(name, Integer.MAX_VALUE);
    }
}
