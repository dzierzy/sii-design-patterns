package lab.patterns.behavioral.cor.purchase;

public class Secretary extends Approver {


    public Secretary(String name) {
        super(name, 50);
    }
}