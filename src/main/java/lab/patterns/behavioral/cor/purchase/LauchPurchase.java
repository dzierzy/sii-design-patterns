package lab.patterns.behavioral.cor.purchase;

public class LauchPurchase {

    public static void main(String[] args) {
        System.out.println("le't processa purchase");

        Purchase p = new Purchase(243162361, "samochod dla sprzedawcy");

        getApprovers().approve(p);


        System.out.println("purchase state: " + p);
    }

    private static Approver getApprovers(){
        Secretary secretary = new Secretary("Sekretarka Basia");
        Director director = new Director("Dyrektor Jacek");
        President president = new President("Prezes Grzegorz");

        secretary.registerNext(director).registerNext(president);

        return secretary;
    }
}
